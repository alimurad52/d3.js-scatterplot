
var margin = {top: 20, right: 20, bottom: 50, left: 50};
var width = 700 - margin.left - margin.right;
var height = 500 - margin.top - margin.bottom;

var dataset = [
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.7, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.1, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.1, "species": "setosa"},
  {"petalLength": 1.1, "petalWidth": 0.1, "species": "setosa"},
  {"petalLength": 1.2, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.7, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.7, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.0, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.7, "petalWidth": 0.5, "species": "setosa"},
  {"petalLength": 1.9, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.1, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.2, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.1, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.3, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.6, "species": "setosa"},
  {"petalLength": 1.9, "petalWidth": 0.4, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.3, "species": "setosa"},
  {"petalLength": 1.6, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.5, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 1.4, "petalWidth": 0.2, "species": "setosa"},
  {"petalLength": 4.7, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.9, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.0, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.6, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.7, "petalWidth": 1.6, "species": "versicolor"},
  {"petalLength": 3.3, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 4.6, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 3.9, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 3.5, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 4.2, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.0, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 4.7, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 3.6, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.4, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.1, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 3.9, "petalWidth": 1.1, "species": "versicolor"},
  {"petalLength": 4.8, "petalWidth": 1.8, "species": "versicolor"},
  {"petalLength": 4.0, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.9, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.7, "petalWidth": 1.2, "species": "versicolor"},
  {"petalLength": 4.3, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.4, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 4.8, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 5.0, "petalWidth": 1.7, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 3.5, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 3.8, "petalWidth": 1.1, "species": "versicolor"},
  {"petalLength": 3.7, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 3.9, "petalWidth": 1.2, "species": "versicolor"},
  {"petalLength": 5.1, "petalWidth": 1.6, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.5, "petalWidth": 1.6, "species": "versicolor"},
  {"petalLength": 4.7, "petalWidth": 1.5, "species": "versicolor"},
  {"petalLength": 4.4, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.1, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.0, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.4, "petalWidth": 1.2, "species": "versicolor"},
  {"petalLength": 4.6, "petalWidth": 1.4, "species": "versicolor"},
  {"petalLength": 4.0, "petalWidth": 1.2, "species": "versicolor"},
  {"petalLength": 3.3, "petalWidth": 1.0, "species": "versicolor"},
  {"petalLength": 4.2, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.2, "petalWidth": 1.2, "species": "versicolor"},
  {"petalLength": 4.2, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 4.3, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 3.0, "petalWidth": 1.1, "species": "versicolor"},
  {"petalLength": 4.1, "petalWidth": 1.3, "species": "versicolor"},
  {"petalLength": 6.0, "petalWidth": 2.5, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 1.9, "species": "virginica"},
  {"petalLength": 5.9, "petalWidth": 2.1, "species": "virginica"},
  {"petalLength": 5.6, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 5.8, "petalWidth": 2.2, "species": "virginica"},
  {"petalLength": 6.6, "petalWidth": 2.1, "species": "virginica"},
  {"petalLength": 4.5, "petalWidth": 1.7, "species": "virginica"},
  {"petalLength": 6.3, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 5.8, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 6.1, "petalWidth": 2.5, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 2.0, "species": "virginica"},
  {"petalLength": 5.3, "petalWidth": 1.9, "species": "virginica"},
  {"petalLength": 5.5, "petalWidth": 2.1, "species": "virginica"},
  {"petalLength": 5.0, "petalWidth": 2.0, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 2.4, "species": "virginica"},
  {"petalLength": 5.3, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.5, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 6.7, "petalWidth": 2.2, "species": "virginica"},
  {"petalLength": 6.9, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.0, "petalWidth": 1.5, "species": "virginica"},
  {"petalLength": 5.7, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 4.9, "petalWidth": 2.0, "species": "virginica"},
  {"petalLength": 6.7, "petalWidth": 2.0, "species": "virginica"},
  {"petalLength": 4.9, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 5.7, "petalWidth": 2.1, "species": "virginica"},
  {"petalLength": 6.0, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 4.8, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 4.9, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 5.6, "petalWidth": 2.1, "species": "virginica"},
  {"petalLength": 5.8, "petalWidth": 1.6, "species": "virginica"},
  {"petalLength": 6.1, "petalWidth": 1.9, "species": "virginica"},
  {"petalLength": 6.4, "petalWidth": 2.0, "species": "virginica"},
  {"petalLength": 5.6, "petalWidth": 2.2, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 1.5, "species": "virginica"},
  {"petalLength": 5.6, "petalWidth": 1.4, "species": "virginica"},
  {"petalLength": 6.1, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.6, "petalWidth": 2.4, "species": "virginica"},
  {"petalLength": 5.5, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 4.8, "petalWidth": 1.8, "species": "virginica"},
  {"petalLength": 5.4, "petalWidth": 2.1, "species": "virginica"},
  {"petalLength": 5.6, "petalWidth": 2.4, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 1.9, "species": "virginica"},
  {"petalLength": 5.9, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.7, "petalWidth": 2.5, "species": "virginica"},
  {"petalLength": 5.2, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.0, "petalWidth": 1.9, "species": "virginica"},
  {"petalLength": 5.2, "petalWidth": 2.0, "species": "virginica"},
  {"petalLength": 5.4, "petalWidth": 2.3, "species": "virginica"},
  {"petalLength": 5.1, "petalWidth": 1.8, "species": "virginica"}
]

var xScale = d3.scale.linear()
         .domain([0, d3.max(dataset, function(e){
          return e.petalLength;
        })])
         .range([0, width])
       
var yScale = d3.scale.linear()
         .domain([0, d3.max(dataset, function(e){
          return e.petalWidth;
        })])
             .range([height, 0])
             

var xAxis = d3.svg.axis()
        .scale(xScale) 
        .orient('bottom') 
        .ticks(10) 
        
     
var yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left')
        .ticks(10)

var svg = d3.select('body')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
      
      
     
var color = d3.scale.category20()
         

var circles = svg.selectAll('circle')
     .data(dataset)
     .enter()
     .append('circle')
     .attr('cx', function(e){
      return xScale(e.petalLength);
    })
     .attr('cy', function(e){
      return yScale(e.petalWidth);
    })
     .attr('r', 5)
     .attr('fill', function(e){
      return color(e.species)
    })
         
    
svg.append('g')
   .attr('class', 'x axis') 
   .attr('transform', 'translate(0,'+ height+')')
   .call(xAxis)
   .append('text')
   .attr('class', 'label')
   .attr('x', width)
   .attr('y', -6)
   .style('text-anchor', 'end')
   .text('Petal Length (cm)')
 
svg.append('g')
   .attr('class', 'y axis')
   .call(yAxis)
   .append('text')
   .attr('class', 'label')
   .attr('transform', 'rotate(-90)')
   .attr('dy', '0.9em')
   .style('text-anchor', 'end')
   .text('Petal Width (cm)')
   
function petalSetosa() {
  svg.selectAll('circle').attr('r', function(e) {
    if(e.species == "setosa") {
      return 0
    }
    else return 5
  })
}
function petalVersicolor() {
  svg.selectAll('circle').attr('r', function(e) {
    if(e.species == "versicolor") {
      return 0
    }
    else return 5
  })
}
function petalVirginica() {
  svg.selectAll('circle').attr('r', function(e) {
    if(e.species == "virginica") {
      return 0
    }
    else return 5
  })
}
function showall() {
  svg.selectAll('circle').attr('r', function(e) {
    return 5
  })
}