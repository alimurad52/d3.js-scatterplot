
var margin = {top: 20, right: 20, bottom: 50, left: 50};
var width = 700 - margin.left - margin.right;
var height = 500 - margin.top - margin.bottom;

var dataset = [
  {"sepalLength": 5.1, "sepalWidth": 3.5, "species": "setosa"},
  {"sepalLength": 4.9, "sepalWidth": 3.0, "species": "setosa"},
  {"sepalLength": 4.7, "sepalWidth": 3.2, "species": "setosa"},
  {"sepalLength": 4.6, "sepalWidth": 3.1, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.6, "species": "setosa"},
  {"sepalLength": 5.4, "sepalWidth": 3.9, "species": "setosa"},
  {"sepalLength": 4.6, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 4.4, "sepalWidth": 2.9, "species": "setosa"},
  {"sepalLength": 4.9, "sepalWidth": 3.1, "species": "setosa"},
  {"sepalLength": 5.4, "sepalWidth": 3.7, "species": "setosa"},
  {"sepalLength": 4.8, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 4.8, "sepalWidth": 3.0, "species": "setosa"},
  {"sepalLength": 4.3, "sepalWidth": 3.0, "species": "setosa"},
  {"sepalLength": 5.8, "sepalWidth": 4.0, "species": "setosa"},
  {"sepalLength": 5.7, "sepalWidth": 4.4, "species": "setosa"},
  {"sepalLength": 5.4, "sepalWidth": 3.9, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.5, "species": "setosa"},
  {"sepalLength": 5.7, "sepalWidth": 3.8, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.8, "species": "setosa"},
  {"sepalLength": 5.4, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.7, "species": "setosa"},
  {"sepalLength": 4.6, "sepalWidth": 3.6, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.3, "species": "setosa"},
  {"sepalLength": 4.8, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.0, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 5.2, "sepalWidth": 3.5, "species": "setosa"},
  {"sepalLength": 5.2, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 4.7, "sepalWidth": 3.2, "species": "setosa"},
  {"sepalLength": 4.8, "sepalWidth": 3.1, "species": "setosa"},
  {"sepalLength": 5.4, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 5.2, "sepalWidth": 4.1, "species": "setosa"},
  {"sepalLength": 5.5, "sepalWidth": 4.2, "species": "setosa"},
  {"sepalLength": 4.9, "sepalWidth": 3.1, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.2, "species": "setosa"},
  {"sepalLength": 5.5, "sepalWidth": 3.5, "species": "setosa"},
  {"sepalLength": 4.9, "sepalWidth": 3.6, "species": "setosa"},
  {"sepalLength": 4.4, "sepalWidth": 3.0, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.4, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.5, "species": "setosa"},
  {"sepalLength": 4.5, "sepalWidth": 2.3, "species": "setosa"},
  {"sepalLength": 4.4, "sepalWidth": 3.2, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.5, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.8, "species": "setosa"},
  {"sepalLength": 4.8, "sepalWidth": 3.0, "species": "setosa"},
  {"sepalLength": 5.1, "sepalWidth": 3.8, "species": "setosa"},
  {"sepalLength": 4.6, "sepalWidth": 3.2, "species": "setosa"},
  {"sepalLength": 5.3, "sepalWidth": 3.7, "species": "setosa"},
  {"sepalLength": 5.0, "sepalWidth": 3.3, "species": "setosa"},
  {"sepalLength": 7.0, "sepalWidth": 3.2, "species": "versicolor"},
  {"sepalLength": 6.4, "sepalWidth": 3.2, "species": "versicolor"},
  {"sepalLength": 6.9, "sepalWidth": 3.1, "species": "versicolor"},
  {"sepalLength": 5.5, "sepalWidth": 2.3, "species": "versicolor"},
  {"sepalLength": 6.5, "sepalWidth": 2.8, "species": "versicolor"},
  {"sepalLength": 5.7, "sepalWidth": 2.8, "species": "versicolor"},
  {"sepalLength": 6.3, "sepalWidth": 3.3, "species": "versicolor"},
  {"sepalLength": 4.9, "sepalWidth": 2.4, "species": "versicolor"},
  {"sepalLength": 6.6, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 5.2, "sepalWidth": 2.7, "species": "versicolor"},
  {"sepalLength": 5.0, "sepalWidth": 2.0, "species": "versicolor"},
  {"sepalLength": 5.9, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 6.0, "sepalWidth": 2.2, "species": "versicolor"},
  {"sepalLength": 6.1, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 5.6, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 6.7, "sepalWidth": 3.1, "species": "versicolor"},
  {"sepalLength": 5.6, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 5.8, "sepalWidth": 2.7, "species": "versicolor"},
  {"sepalLength": 6.2, "sepalWidth": 2.2, "species": "versicolor"},
  {"sepalLength": 5.6, "sepalWidth": 2.5, "species": "versicolor"},
  {"sepalLength": 5.9, "sepalWidth": 3.2, "species": "versicolor"},
  {"sepalLength": 6.1, "sepalWidth": 2.8, "species": "versicolor"},
  {"sepalLength": 6.3, "sepalWidth": 2.5, "species": "versicolor"},
  {"sepalLength": 6.1, "sepalWidth": 2.8, "species": "versicolor"},
  {"sepalLength": 6.4, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 6.6, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 6.8, "sepalWidth": 2.8, "species": "versicolor"},
  {"sepalLength": 6.7, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 6.0, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 5.7, "sepalWidth": 2.6, "species": "versicolor"},
  {"sepalLength": 5.5, "sepalWidth": 2.4, "species": "versicolor"},
  {"sepalLength": 5.5, "sepalWidth": 2.4, "species": "versicolor"},
  {"sepalLength": 5.8, "sepalWidth": 2.7, "species": "versicolor"},
  {"sepalLength": 6.0, "sepalWidth": 2.7, "species": "versicolor"},
  {"sepalLength": 5.4, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 6.0, "sepalWidth": 3.4, "species": "versicolor"},
  {"sepalLength": 6.7, "sepalWidth": 3.1, "species": "versicolor"},
  {"sepalLength": 6.3, "sepalWidth": 2.3, "species": "versicolor"},
  {"sepalLength": 5.6, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 5.5, "sepalWidth": 2.5, "species": "versicolor"},
  {"sepalLength": 5.5, "sepalWidth": 2.6, "species": "versicolor"},
  {"sepalLength": 6.1, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 5.8, "sepalWidth": 2.6, "species": "versicolor"},
  {"sepalLength": 5.0, "sepalWidth": 2.3, "species": "versicolor"},
  {"sepalLength": 5.6, "sepalWidth": 2.7, "species": "versicolor"},
  {"sepalLength": 5.7, "sepalWidth": 3.0, "species": "versicolor"},
  {"sepalLength": 5.7, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 6.2, "sepalWidth": 2.9, "species": "versicolor"},
  {"sepalLength": 5.1, "sepalWidth": 2.5, "species": "versicolor"},
  {"sepalLength": 5.7, "sepalWidth": 2.8, "species": "versicolor"},
  {"sepalLength": 6.3, "sepalWidth": 3.3, "species": "virginica"},
  {"sepalLength": 5.8, "sepalWidth": 2.7, "species": "virginica"},
  {"sepalLength": 7.1, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 6.3, "sepalWidth": 2.9, "species": "virginica"},
  {"sepalLength": 6.5, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 7.6, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 4.9, "sepalWidth": 2.5, "species": "virginica"},
  {"sepalLength": 7.3, "sepalWidth": 2.9, "species": "virginica"},
  {"sepalLength": 6.7, "sepalWidth": 2.5, "species": "virginica"},
  {"sepalLength": 7.2, "sepalWidth": 3.6, "species": "virginica"},
  {"sepalLength": 6.5, "sepalWidth": 3.2, "species": "virginica"},
  {"sepalLength": 6.4, "sepalWidth": 2.7, "species": "virginica"},
  {"sepalLength": 6.8, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 5.7, "sepalWidth": 2.5, "species": "virginica"},
  {"sepalLength": 5.8, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 6.4, "sepalWidth": 3.2, "species": "virginica"},
  {"sepalLength": 6.5, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 7.7, "sepalWidth": 3.8, "species": "virginica"},
  {"sepalLength": 7.7, "sepalWidth": 2.6, "species": "virginica"},
  {"sepalLength": 6.0, "sepalWidth": 2.2, "species": "virginica"},
  {"sepalLength": 6.9, "sepalWidth": 3.2, "species": "virginica"},
  {"sepalLength": 5.6, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 7.7, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 6.3, "sepalWidth": 2.7, "species": "virginica"},
  {"sepalLength": 6.7, "sepalWidth": 3.3, "species": "virginica"},
  {"sepalLength": 7.2, "sepalWidth": 3.2, "species": "virginica"},
  {"sepalLength": 6.2, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 6.1, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 6.4, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 7.2, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 7.4, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 7.9, "sepalWidth": 3.8, "species": "virginica"},
  {"sepalLength": 6.4, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 6.3, "sepalWidth": 2.8, "species": "virginica"},
  {"sepalLength": 6.1, "sepalWidth": 2.6, "species": "virginica"},
  {"sepalLength": 7.7, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 6.3, "sepalWidth": 3.4, "species": "virginica"},
  {"sepalLength": 6.4, "sepalWidth": 3.1, "species": "virginica"},
  {"sepalLength": 6.0, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 6.9, "sepalWidth": 3.1, "species": "virginica"},
  {"sepalLength": 6.7, "sepalWidth": 3.1, "species": "virginica"},
  {"sepalLength": 6.9, "sepalWidth": 3.1, "species": "virginica"},
  {"sepalLength": 5.8, "sepalWidth": 2.7, "species": "virginica"},
  {"sepalLength": 6.8, "sepalWidth": 3.2, "species": "virginica"},
  {"sepalLength": 6.7, "sepalWidth": 3.3, "species": "virginica"},
  {"sepalLength": 6.7, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 6.3, "sepalWidth": 2.5, "species": "virginica"},
  {"sepalLength": 6.5, "sepalWidth": 3.0, "species": "virginica"},
  {"sepalLength": 6.2, "sepalWidth": 3.4, "species": "virginica"},
  {"sepalLength": 5.9, "sepalWidth": 3.0, "species": "virginica"}
]

var xScale = d3.scale.linear()
         .domain([0, d3.max(dataset, function(e){
          return e.sepalLength;
        })])
         .range([0, width])
     
var yScale = d3.scale.linear()
         .domain([0, d3.max(dataset, function(e){
          return e.sepalWidth;
        })])
             .range([height, 0])
             

var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient('bottom')
        .ticks(10) 
        
      
var yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left')
        .ticks(10)

var svg = d3.select('body')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

var color = d3.scale.category10()
         

var circles = svg.selectAll('circle')
     .data(dataset)
     .enter()
     .append('circle')
     .attr('cx', function(e){
      return xScale(e.sepalLength);
    })
     .attr('cy', function(e){
      return yScale(e.sepalWidth);
    })
     .attr('r', 5)
     .attr('fill', function(e){
      return color(e.species)
    })
         
 
svg.append('g')
   .attr('class', 'x axis') 
   .attr('transform', 'translate(0,'+ height+')')
   .call(xAxis)
   .append('text')
   .attr('class', 'label')
   .attr('x', width)
   .attr('y', -6)
   .style('text-anchor', 'end')
   .text('Sepal Length (cm)')

svg.append('g')
   .attr('class', 'y axis')
   .call(yAxis)
   .append('text')
   .attr('class', 'label')
   .attr('transform', 'rotate(-90)')
   .attr('dy', '0.9em')
   .style('text-anchor', 'end')
   .text('Sepal Width (cm)')

function sepalSetosa() {
  svg.selectAll('circle').attr('r', function(e) {
    if(e.species == "setosa") {
      return 0
    }
    else return 5
  })
}
function sepalVersicolor() {
  svg.selectAll('circle').attr('r', function(e) {
    if(e.species == "versicolor") {
      return 0
    }
    else return 5
  })
}
function sepalVirginica() {
  svg.selectAll('circle').attr('r', function(e) {
    if(e.species == "virginica") {
      return 0
    }
    else return 5
  })
}
function showall() {
  svg.selectAll('circle').attr('r', function(e) {
    return 5
  })
}
   